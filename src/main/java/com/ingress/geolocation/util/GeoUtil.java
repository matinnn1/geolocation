package com.ingress.geolocation.util;

public class GeoUtil {
    private static final double EARTH_RADIUS = 6371000; // Earth's radius in meters
    public static double calculateHaversineDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        // Convert latitude and longitude from degrees to radians
        double startLatRadRad = Math.toRadians(startLatitude);
        double startLonRad = Math.toRadians(startLongitude);
        double endLatRad = Math.toRadians(endLatitude);
        double endLonRad = Math.toRadians(endLongitude);

        // Haversine formula
        double dLat = endLatRad - startLatRadRad;
        double dLon = endLonRad - startLonRad;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(startLatRadRad) * Math.cos(endLatRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // Calculate the distance in meters
        return EARTH_RADIUS * c;
    }
}
