package com.ingress.geolocation.service.impl;

import com.ingress.geolocation.entity.Client;
import com.ingress.geolocation.repository.ClientRepository;
import com.ingress.geolocation.service.inter.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClientManager implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public Client findById(int id) {
        return clientRepository.findById(id).orElse(new Client());
    }
}
