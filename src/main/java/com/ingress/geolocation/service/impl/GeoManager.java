package com.ingress.geolocation.service.impl;

import com.ingress.geolocation.entity.Client;
import com.ingress.geolocation.entity.Company;
import com.ingress.geolocation.service.inter.ClientService;
import com.ingress.geolocation.service.inter.CompanyService;
import com.ingress.geolocation.service.inter.GeoService;
import com.ingress.geolocation.util.GeoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GeoManager implements GeoService {
    private final CompanyService companyService;
    private final ClientService clientService;

    @Override
    public double getDistanceByClinetIdAndCompanyId(int clientId, int companyId){
        Client client = clientService.findById(clientId);
        Company company = companyService.findById(companyId);

        return GeoUtil.calculateHaversineDistance(client.getLatitude(),client.getLongitude(), company.getLatitude(), company.getLongitude());
    }
}
