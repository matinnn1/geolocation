package com.ingress.geolocation.service.impl;

import com.ingress.geolocation.entity.Company;
import com.ingress.geolocation.repository.CompanyRepository;
import com.ingress.geolocation.service.inter.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyManager implements CompanyService {
    private final CompanyRepository companyRepository;
    @Override
    public Company findById(int id) {
        return companyRepository.findById(id).orElse(new Company());
    }
}
