package com.ingress.geolocation.service.inter;

import com.ingress.geolocation.entity.Client;

import java.util.Optional;

public interface ClientService {
    Client findById(int id);
}
