package com.ingress.geolocation.service.inter;

import com.ingress.geolocation.entity.Company;

import java.util.Optional;

public interface CompanyService {
    Company findById(int id);
}
