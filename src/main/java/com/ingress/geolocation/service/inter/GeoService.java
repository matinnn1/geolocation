package com.ingress.geolocation.service.inter;

public interface GeoService {
    double getDistanceByClinetIdAndCompanyId(int clientId, int companyId);
}
