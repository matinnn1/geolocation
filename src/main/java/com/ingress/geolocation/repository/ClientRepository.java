package com.ingress.geolocation.repository;

import com.ingress.geolocation.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer> {
}
