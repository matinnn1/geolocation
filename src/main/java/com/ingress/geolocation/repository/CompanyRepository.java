package com.ingress.geolocation.repository;

import com.ingress.geolocation.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company,Integer> {
}
