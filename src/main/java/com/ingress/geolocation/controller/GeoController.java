package com.ingress.geolocation.controller;

import com.ingress.geolocation.service.impl.GeoManager;
import com.ingress.geolocation.service.inter.GeoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/geo")
@RequiredArgsConstructor
public class GeoController {
    private final GeoService geoService;

    @GetMapping
    public String get( @RequestParam("clientId") int clientId,
                            @RequestParam("companyId") int companyId) {

        double distance = geoService.getDistanceByClinetIdAndCompanyId(clientId, companyId);

        return "Distance between client's location and company location: " + distance + " meters";
    }
}



